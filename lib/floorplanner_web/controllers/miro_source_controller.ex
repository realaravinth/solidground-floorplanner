defmodule FloorplannerWeb.MiroSourceController do
  use FloorplannerWeb, :controller
  alias FloorplannerWeb.UserAuth
  alias Commanded.Generator.{Project, New}

  alias Floorplanner.Repo
  alias Floorplanner.Accounts.User
  alias Floorplanner.Boards
  alias Floorplanner.Commanded

  import Config

  def new(conn, _params) do
    changeset = Boards.changeset(%Boards{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(
        conn,
        %{"boards" => %{"name" => name, "api_key" => api_key, "board_id" => board_id}} = params
      ) do
    user = conn.assigns.current_user

    changeset =
      %Boards{}
      |> Boards.changeset(%{name: name, api_key: api_key, board_id: board_id, user_id: user.id})

    {:ok, board} =
      changeset
      |> Repo.insert()

    path = Commanded.get_path(user, board, name)
    IO.puts("#{path}")
    opts = [access_token: api_key, miro: board_id]
    Commanded.generate(path, New, :project_path, opts)

    render(conn, "new.html", changeset: changeset)
  end
end
