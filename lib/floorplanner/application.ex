defmodule Floorplanner.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Floorplanner.Repo,
      # Start the Telemetry supervisor
      FloorplannerWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Floorplanner.PubSub},
      # Start the Endpoint (http/https)
      FloorplannerWeb.Endpoint
      # Start a worker by calling: Floorplanner.Worker.start_link(arg)
      # {Floorplanner.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Floorplanner.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    FloorplannerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
