defmodule Floorplanner.Boards do
  @moduledoc """
  A Miro board. Contains visitor-provided MIRO API key, a board name and a
  board ID.
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "boards" do
    field :api_key, :string
    field :board_id, :string
    field :name, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(boards, attrs \\ %{}) do
    boards
    |> cast(attrs, [:board_id, :user_id, :api_key, :name])
    |> validate_required([:board_id, :user_id, :api_key, :name])
  end
end
