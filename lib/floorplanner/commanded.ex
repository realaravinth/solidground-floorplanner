defmodule Floorplanner.Commanded do
  alias Commanded.Generator
  alias Commanded.Generator.{Project, New}
  alias Commanded.Generator.Source.Miro

  @moduledoc """
  Utilities to work with Commanded library
  """

  @doc """
  Utility function to generate codegen output path from a user ID, board
  ID(database assigned ID) and base path provided in configuration
  """
  def get_path(user, board, name) do
    Application.get_env(:floorplanner, :codegen, :output_base_path)[:output_base_path]
    |> Path.join("#{user.id}")
    |> Path.join("#{board.id}")
    |> Path.join("#{name |> String.replace(" ", "-")}")
  end

  @doc """
  Utility function to codegen from Miro board.
  Uses groundwork/floorplanner-generator-prototype under the hood to download
  data from Miro and generate commanded/commanded[1] code.

  [0]: https://codeberg.org/groundwork/floorplanner-generator-prototype.git
  [1]: https://github.com/commanded/commanded
  """
  def generate(base_path, generator, path, opts) do
    base_path
    |> Project.new(opts)
    |> build_model()
    |> generator.prepare_project()
    |> Generator.put_binding()
    |> generator.generate()
  end

  defp build_model(%Project{} = project) do
    %Project{app_mod: app_mod, opts: opts} = project

    case Keyword.get(opts, :miro) do
      board_id when is_binary(board_id) ->
        Project.build_model(project, Miro, namespace: app_mod, board_id: board_id)

      nil ->
        project
    end
  end
end
