defmodule FloorplannerWeb.PageControllerTest do
  use FloorplannerWeb.ConnCase

  setup :register_and_log_in_user

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to Floorplanner"
  end
end
