defmodule FloorplannerWeb.MiroSourceControllerTest do
  import Ecto.Query, warn: false
  use FloorplannerWeb.ConnCase
  alias Floorplanner.Repo
  alias Floorplanner.Boards
  alias Floorplanner.Commanded

  setup :register_and_log_in_user

  test "GET /source/miro/new", %{conn: conn} do
    conn = get(conn, "/source/miro/new")
    assert html_response(conn, 200) =~ "Import Miro Board"
    assert html_response(conn, 200) =~ "Board ID"
    assert html_response(conn, 200) =~ "API Key"
  end

  test "POST /source/miro/new", %{conn: conn, user: user} do
    name = "test board"
    api_key = System.get_env("MIRO_ACCESS_TOKEN")
    board_id = System.get_env("MIRO_BOARD_ID")

    conn =
      post(conn, "/source/miro/new", %{
        "boards" => %{"board_id" => board_id, "api_key" => api_key, "name" => name}
      })

    assert html_response(conn, 200) =~ "Import Miro Board"
    assert html_response(conn, 200) =~ "Board ID"
    assert html_response(conn, 200) =~ "API Key"

    board = Repo.get_by(Boards, %{api_key: api_key})

    assert board == Repo.get_by(Boards, %{board_id: board_id})
    assert board.board_id == board_id
    assert board.api_key == api_key
    assert user.id == board.user_id

    path = Commanded.get_path(user, board, name)

    # verify codegen
    assert File.dir?(path) == true
    assert File.dir?(Path.join(path, "priv/repo/migrations/")) == true
    assert File.dir?(Path.join(path, "mix.exs")) == false
    assert File.dir?(Path.join(path, "lib/" <> name <> "/app.ex")) == false
    assert File.dir?(Path.join(path, "lib/" <> name <> "/repo.ex")) == false
    assert File.dir?(Path.join(path, "lib/" <> name <> "/router.ex")) == false
    assert File.dir?(Path.join(path, "lib/" <> name <> "/router.ex")) == false
  end
end
